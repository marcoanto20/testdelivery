﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
//using System.Web.Script.Serialization;
using System.IO;

namespace DeliverBox
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 1;
            int cnt = 1;
            int days = 2;
            int capacity = 20;
            string[] airPortCodes = { "YUL", "YYZ", "YYC", "YVR" };
            string departures = airPortCodes[0];
            List<Order> lOrders = new List<Order>();
            Dictionary<string, Dictionary<string, string>> values;
            using (StreamReader r = new StreamReader("D://test//coding-assigment-orders.json"))
            {
                string json = r.ReadToEnd();
                values = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string,string>>>(json);
            }
            foreach (KeyValuePair<string, Dictionary<string, string>> order in values) {
                Order iorder = new Order();
                iorder.orderId = order.Key;
                iorder.destination = order.Value["destination"];
                iorder.status = "ready";
                lOrders.Add(iorder);
            }

// User story 1

            while (i < days + 1 )
            {
                foreach (string code in airPortCodes)
                {
                    Flight iFlight = new Flight();
                    if (code != departures)
                    {
                        iFlight.departure = airPortCodes[0];
                        iFlight.arrival = code;
                        Console.WriteLine("Flight: " + cnt + " departure: " + iFlight.departure + " arrival: " + iFlight.arrival + " day: " + i + " ");
                        cnt++;
                    }
                }
                i++;
            }


// User story 2
            int y = 1;
            i = 1;
            cnt = 1;
            while (i < days + 1)
            {
                foreach (string code in airPortCodes)
                {
                    Flight iFlight = new Flight();
                    if (code != departures)
                    {
                        iFlight.departure = airPortCodes[0];
                        iFlight.arrival = code;
                        y = 1;
                        foreach (Order item in lOrders) {
                            if (item.status == "ready")
                            {
                                if (y <= capacity)
                                {
                                    Console.WriteLine("order: " + item.orderId + " Flight: " + cnt + " departure: " + iFlight.departure + " arrival: " + iFlight.arrival + " day: " + i + " ");
                                    item.status = "delivered";
                                }
                                else {
                                    break;
                                }
                                y++;
                            }                         
                        }
                        cnt++;
                    }
                }
                i++;
            }
            foreach (Order item in lOrders)
            {
                if (item.status == "ready")
                {
                    Console.WriteLine("order: " + item.orderId + " Flight: not scheduled");
                }
                y++;
            }
            Console.ReadLine();
        }
    }
    class Flight {
        public string departure { get; set; }
        public string arrival { get; set; }
        public int day { get; set; }
    }
    class Order {
        public string orderId { get; set; }
        public string destination { get; set; }
        public string status { get; set; }
    }
}
